"""discussion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    # By using 'include', we are attaching All the urls from a specific package to the specific path that is declared in this file.
    # Example, in order to see the index page of todolist package we have to visit this url: localhost:8000/todolist/
    # path('', function, alias) to run pa din localhost 8000
    path('todolist/', include('todolist.urls')),
    path('admin/', admin.site.urls),
]
